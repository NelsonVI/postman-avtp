Primer repo Postman AVTP

Pasos para correr con newman
Precondiciones: Tener previamente newman instalado. 

1) Bajar los archivos AVTP.json y environmentsPostman.json de este repositorio
2) En la carpeta donde se bajo ambos archivos correr con la linea de comandos la siguiente linea:
	"newman run AVTP.json -e environmentsPostman.json" 

3) Esperar resultados.
